# Code Record At Will Day2
* 977 Squares of a Sorted Array
* 209 Minimum Size Subarray Sum
* 59 Spiral Matrix II

## Problem 977

Given an integer array nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.

### hints:
* The array of squares of non-decreasing numbers looks like "\/", it goes down and then up.
* Use two pointers: one points at the head of the array of squares and the other points to the tail of it.
* Compare them and record and move the smaller one.

``` java
class Solution {
    public int[] sortedSquares(int[] nums) {
        for(int i = 0 ;i < nums.length ;i++){
            nums[i] *= nums[i];
        }
        int[] res = new int[nums.length];
        int left = 0;
        int right = nums.length - 1;
        int index = nums.length - 1;
        while(left <= right){
            if(nums[left]>nums[right]){
                res[index] = nums[left];
                left++;
            }else{
                res[index] = nums[right];
                right--;
            }
            index--;
        }
        return res; 
    }
}
```

## Problem 209 

Given an array of positive integers nums and a positive integer target, return the minimal length of a subarray whose sum is greater than or equal to target. If there is no such subarray, return 0 instead.

### hints
* Use sliding window to control the size of the section of the array.

``` java
class Solution {
    public int minSubArrayLen(int target, int[] nums) {
        int left = 0;
        int right = 0;
        int min = nums.length + 1;
        while(true){
            if(target <= 0){
                min = Math.min(min,right - left);
                
            }
            if(target > 0){
                if(right < nums.length){
                    target -= nums[right++];
                }else{
                    break;
                }
            }else{
                if(left < nums.length){
                    target += nums[left++];
                }else{
                    break;
                }
            }
        }
        return (min == nums.length + 1) ? 0 : min;
    }
}
```

## Problem 59

Given a positive integer n, generate an n x n matrix filled with elements from 1 to n2 in spiral order.


### hints
* Use Recursion
* Find the invariant

``` java
class Solution {
    public int[][] generateMatrix(int n) {
        int[][] ans = new int[n][n];
        fillMatrix(1,0,n-1,0,n-1,ans);
        return ans;
    }
    public void fillMatrix(int startNum, int left, int right, int up, int down, int [][] matrix){
        if(left > right){
            return;
        }
        if(left == right){
            matrix[left][left] = startNum;
            return;
        }
        for(int i = left; i < right; i++){
            matrix[up][i] = startNum++;
        }
        for(int i = up; i < down; i++){
            matrix[i][right] = startNum++;
        }
        for(int i = right; i > left; i--){
            matrix[down][i] = startNum++;
        }
        for(int i = down; i > up; i--){
            matrix[i][left] = startNum++;
        }
        fillMatrix(startNum, left + 1, right - 1, up + 1, down - 1, matrix);
    }
}
```