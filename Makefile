# 指定编译器
CXX = g++

# 指定编译参数
CXXFLAGS = -std=c++14 -Wall

# 指定目标文件
TARGET = test

all: $(TARGET)

$(TARGET): test.cpp
	$(CXX) $(CXXFLAGS) -o $(TARGET) test.cpp

clean:
	rm -f $(TARGET)