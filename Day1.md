# Code Record At Will Day1
* 702 Binary Search
* 704 Remove Element

## Problem 702

Given an integer array nums and an integer val, remove all occurrences of val in nums in-place. The order of the elements may be changed. Then return the number of elements in nums which are not equal to val.

Consider the number of elements in nums which are not equal to val be k, to get accepted, you need to do the following things:

Change the array nums such that the first k elements of nums contain the elements which are not equal to val. The remaining elements of nums are not important as well as the size of nums.
Return k.

### hints:
* Use two pointers method. The pointers are borders. The elements before the left pointer or after the right one are exluded.
* Every time we choose the middle elements between the left pointer and right pointer, compare it with the target number. if it is the target, congradulations! Return the middle index right away. If it smaller than the target, which means all the element before it (it included), are smaller than target. So we can move the left pointer to the first element on the right of the element.
* We can do this again and again until there is no element between the pointers, in other word, left pointer is on the right of right pointer. Then we jump out of the loop and return -1;

``` java
class Solution {
    public int removeElement(int[] nums, int val) {
        int i =0;
        for(int j = 0; j < nums.length; j++){
            if(nums[j] != val){
                nums[i] = nums[j];
                i++;
            }
        }
        return i;
    }
} 
```
## Problem 704

Given an integer array nums and an integer val, remove all occurrences of val in nums in-place. The order of the elements may be changed. Then return the number of elements in nums which are not equal to val.

Consider the number of elements in nums which are not equal to val be k, to get accepted, you need to do the following things:

Change the array nums such that the first k elements of nums contain the elements which are not equal to val. The remaining elements of nums are not important as well as the size of nums.
Return k.

### hints:
* Use two pointers method. The pointers are borders. The elements before the left pointer (excluded) are settled, those after the right pointer (included) are elements to be settled. those elements between left pointer and right pointer are useless(elements to be set).

``` java
class Solution {
    public int removeElement(int[] nums, int val) {
        int i = 0;
        int j = 0;
        while(j < nums.length){
            if(nums[j] == val){
            }else{
                nums[i] = nums[j];
                i++;
            }
            j++;
        }
        return i;
    }
}
```

## Problem 283

Given an integer array nums, move all 0's to the end of it while maintaining the relative order of the non-zero elements.

Note that you must do this in-place without making a copy of the array.

``` java
class Solution {
    public void moveZeroes(int[] nums) {
        int i = 0; 
        int j = 0;
        while(j < nums.length){
            if(nums[j] != 0){
                nums[i] = nums[j];
                i++;
            }
            j++;
        }
        for(;i<nums.length;i++)nums[i]=0;
    }
}
```

## Problem 844

Given two strings s and t, return true if they are equal when both are typed into empty text editors. '#' means a backspace character.

Note that after backspacing an empty text, the text will continue empty.

``` java
class Solution {
    public boolean backspaceCompare(String s, String t) {
        return calculateString(s).equals(calculateString(t));
    }

    public String calculateString(String originString){
        char[] originCharArray = originString.toCharArray();
        int i = 0;
        int j = 0;
        while(j < originCharArray.length){
            if(originCharArray[j]=='#'){
                i=(i>0)?i-1:0;
            }else{
                originCharArray[i] = originCharArray[j];
                i++;
            }
            j++;
        }
        return new String(originCharArray,0,i);
    }
}
```