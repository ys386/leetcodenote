# Code Record At Will Day4
* 24 Swap Nodes in Pairs 
* 19 Remove Nth Node From End of 
* Interview Problem 02.07 Intersection of Two Linked Lists LCCI
* 142 Linked List Cycle II

## [Problem 24](https://leetcode.cn/problems/swap-nodes-in-pairs/)

Given a linked list, swap every two adjacent nodes and return its head. You must solve the problem without modifying the values in the list's nodes (i.e., only nodes themselves may be changed.)

### hints:
* Define invariant clearly

Iteration

``` java
class Solution {
    public ListNode swapPairs(ListNode head) {
        if(head==null||head.next==null){
            return head;
        }
        ListNode ans = head.next;
        ListNode cur = head;
        ListNode prev = new ListNode();
        while(cur!=null&&cur.next!=null){
            prev.next = cur.next;
            cur.next = cur.next.next;
            prev.next.next = cur;
            prev = cur;
            cur = cur.next;
        }
        return ans;
    }
}
```

recursion

``` java
class Solution {
    public ListNode swapPairs(ListNode head) {
        if(head==null||head.next==null){
            return head;
        }
        ListNode ans = head.next;
        head.next = swapPairs(head.next.next);
        ans.next = head;
        return ans;
    }
}
```

## [Problem 19](https://leetcode.cn/problems/remove-nth-node-from-end-of-list/)

Given the head of a linked list, remove the nth node from the end of the list and return its head.

### hints
* use two pointer

``` java
class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode dummyHead = new ListNode(-1);
        dummyHead.next = head;
        ListNode fast = head;
        ListNode slow = dummyHead;
        for(int i = 0 ; i < n ; i++){
            fast = fast.next;
        }
        while(fast != null){
            fast = fast.next;
            slow = slow.next;
        }
        slow.next = slow.next.next;
        return dummyHead.next;
    }
}
```

## [Interview Problem 02.07](https://leetcode.cn/problems/intersection-of-two-linked-lists-lcci/)

Given two (singly) linked lists, determine if the two lists intersect. Return the inter­ secting node. Note that the intersection is defined based on reference, not value. That is, if the kth node of the first linked list is the exact same node (by reference) as the jth node of the second linked list, then they are intersecting.

### hints:
* use two pointer

``` java
public class Solution {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        ListNode curA = headA;
        ListNode curB = headB;
        while(curA != null && curB != null){
            curA = curA.next;
            curB = curB.next;
        }
        ListNode shortList;
        ListNode longList;
        ListNode cur;
        int count = 0;
        if(curA != null){
            shortList = headB;
            longList = headA;
            cur = curA;
        }else{
            shortList = headA;
            longList = headB;
            cur = curB;
        }
        while(cur != null){
            cur = cur.next;
            longList = longList.next;
        }
        while(shortList != longList){
            shortList = shortList.next;
            longList = longList.next;
        }
        return shortList;
    }
}
```

## [Problem 142](https://leetcode.cn/problems/linked-list-cycle-ii/)

Given the head of a linked list, return the node where the cycle begins. If there is no cycle, return null.

There is a cycle in a linked list if there is some node in the list that can be reached again by continuously following the next pointer. Internally, pos is used to denote the index of the node that tail's next pointer is connected to (0-indexed). It is -1 if there is no cycle. Note that pos is not passed as a parameter.

Do not modify the linked list.

### hints:
* use two pointer

``` java
public class Solution {
    public ListNode detectCycle(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;
        if(fast == null || fast.next == null){
            return null;
        }else{
            slow = slow.next;
            fast = fast.next.next;
        }
        while(fast!=null && fast.next!=null && slow != fast){
            slow = slow.next;
            fast = fast.next.next;
        }
        if(fast == null || fast.next == null){
            return null;
        }
        while(head != fast){
            head = head.next;
            fast = fast.next;
        }
        return head;
    }
}
```