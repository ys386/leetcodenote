# Code Record At Will Day6
* 242 Valid Anagram
* 349 Intersection of Two Arrays
* 202 Happy Number
* 1 Two Sum

## [Problem 242](https://leetcode.cn/problems/valid-anagram/)

Given two strings s and t, return true if t is an anagram of s, and false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.

``` java
class Solution {
    public boolean isAnagram(String s, String t) {
        int[] count = new int[26];
        for(char c: s.toCharArray()){
            count[c - 'a']++;
        }
        for(char c: t.toCharArray()){
            count[c - 'a']--;
        }
        for(int num: count){
            if(num != 0){
                return false;
            }
        }
        return true;
    }
}
```

## [Problem 349](https://leetcode.cn/problems/intersection-of-two-arrays/)

Given two integer arrays nums1 and nums2, return an array of their intersection. Each element in the result must be unique and you may return the result in any order.

### hints
* use set

``` java
class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        int[] longArray;
        int[] shortArray;
        if(nums1.length > nums2.length){
            longArray = nums1;
            shortArray = nums2;
        }else{
            longArray = nums2;
            shortArray = nums1;
        }
        Set<Integer> set = new HashSet<>();
        Set<Integer> commonSet = new HashSet<>();
        for(int num: shortArray){
            set.add(num);
        }
        for(int num: longArray){
            if(set.contains(num)){
               commonSet.add(num); 
            }
        }
        int[] ans = new int[commonSet.size()];
        int i = 0;
        for(int num:commonSet){
            ans[i++] = num;
        }
        return ans;
    }
}
```

## [Problem 202](https://leetcode.cn/problems/happy-number/)

Write an algorithm to determine if a number n is happy.

A happy number is a number defined by the following process:

Starting with any positive integer, replace the number by the sum of the squares of its digits.
Repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1.
Those numbers for which this process ends in 1 are happy.
Return true if n is a happy number, and false if not.

### hints:
* Consider the final state: get into a loop
* If it is a happy number, it must be 1 -> 1 -> 1 ......
* Otherwise, it will be a -> b -> c -> ... -> a -> b -> c -> ...
* Use set to determine whether it get into a loop.

``` java
class Solution {
    public boolean isHappy(int n) {
        Set<Integer> sumSet = new HashSet<>();
        while(true){
            int sum = getDigitSquareSum(n);
            if(sum == 1){
                return true;
            }
            if(sumSet.contains(sum)){
                return false;
            }
            sumSet.add(sum);
            n = sum;
        }
    }
    public int getDigitSquareSum(int n){
        int sum = 0;
        while(n > 0){
            sum += (n%10)*(n%10);
            n /= 10;
        }
        return sum;
    }
}
```

## [Problem 1](https://leetcode.cn/problems/two-sum/)

Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

### hints:
* Use hashmap

``` java
class Solution {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> indexMap = new HashMap<>();
        for(int i = 0; i < nums.length; i++){
            if(indexMap.containsKey(target - nums[i])){
                return new int[]{indexMap.get(target - nums[i]),i};
            }
            if(!indexMap.containsKey(nums[i])){
                indexMap.put(nums[i], i);
            }
        }
        return new int[]{};
    }
}
```