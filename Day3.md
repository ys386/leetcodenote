# Code Record At Will Day3
* 203 Remove Linked List Elements
* 707 Design Linked List
* 206 Reverse Linked List

## [Problem 203](https://leetcode.cn/problems/remove-linked-list-elements/)

Given the head of a linked list and an integer val, remove all the nodes of the linked list that has Node.val == val, and return the new head.

### hints:
* For those LinkedList that we are not sure which node is the head node, we usually use a dummy node.

``` java
class Solution {
    public ListNode removeElements(ListNode head, int val) {
        ListNode dummy = new ListNode(-1);
        dummy.next = head;
        ListNode pre = dummy;
        while(pre.next!=null){
            if(pre.next.val == val){
                pre.next = pre.next.next;
            }else{
                pre = pre.next;
            }
        }
        return dummy.next;
    }
}
```

## [Problem 707](https://leetcode.cn/problems/design-linked-list/)

Design your implementation of the linked list. You can choose to use a singly or doubly linked list.
A node in a singly linked list should have two attributes: val and next. val is the value of the current node, and next is a pointer/reference to the next node.
If you want to use the doubly linked list, you will need one more attribute prev to indicate the previous node in the linked list. Assume all nodes in the linked list are 0-indexed.

Implement the MyLinkedList class:

MyLinkedList() Initializes the MyLinkedList object.
int get(int index) Get the value of the indexth node in the linked list. If the index is invalid, return -1.
void addAtHead(int val) Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list.
void addAtTail(int val) Append a node of value val as the last element of the linked list.
void addAtIndex(int index, int val) Add a node of value val before the indexth node in the linked list. If index equals the length of the linked list, the node will be appended to the end of the linked list. If index is greater than the length, the node will not be inserted.
void deleteAtIndex(int index) Delete the indexth node in the linked list, if the index is valid.

``` java
class MyLinkedList {
    int size;
    ListNode head;
    ListNode tail;
    class ListNode{
        int val;
        ListNode prev;
        ListNode next;
        public ListNode(int val){
            this.val = val;
            prev = null;
            next = null;
        }
    }

    public MyLinkedList() {
        this.size = 0;
        this.head = null;
        this.tail = null;
    }
    
    public int get(int index) {
        if(size<= index||index<0){
            return -1;
        }
        ListNode cur = head;
        for(int i = 0; i < index; i++){
            cur = cur.next;
        }
        return cur.val;
    }
    
    public void addAtHead(int val) {
        ListNode tmp = new ListNode(val);
        if(size == 0){
            head = tmp;
            tail = tmp;
        }else{
            tmp.next = head;
            head.prev = tmp;
            head = tmp;
        }
        size++;
    }
    
    public void addAtTail(int val) {
        ListNode tmp = new ListNode(val);
        if(size == 0){
            head = tmp;
            tail = tmp;
        }else{
            tmp.prev = tail;
            tail.next = tmp;
            tail = tmp;
        }
        size++;
    }
    
    public void addAtIndex(int index, int val) {
        if(index == 0){
            addAtHead(val);
        }else if(index == size){
            addAtTail(val);
        }else{
            if(index > size){
                return;
            }
            ListNode prev = head;
            for(int i = 0; i < index - 1; i++){
                prev = prev.next;
            }
            ListNode tmp = new ListNode(val);
            tmp.next = prev.next;
            tmp.prev = prev;
            tmp.next.prev = tmp;
            tmp.prev.next = tmp;
            size++;
        }

    }
    
    public void deleteAtIndex(int index) {
        if(!(index>=0 && index<size)){
            return;
        }
        if(size == 1){
            head = null;
            tail = null;
        }else{
            if(index == 0){
                head.next.prev = null;
                head = head.next;
            }else if(index == size - 1){
                tail.prev.next = null;
                tail = tail.prev;
            }else{
                ListNode prev = head;
                for(int i = 0; i < index - 1; i++){
                    prev = prev.next;
                }
                prev.next = prev.next.next;
                prev.next.prev = prev;
            }
            
        }
        size--;
    }
}
```

## [Problem 206](https://leetcode.cn/problems/reverse-linked-list/)

Given the head of a singly linked list, reverse the list, and return the reversed list.

### hints:
* Use easy recursion

``` java
class Solution {
    public ListNode reverseList(ListNode head) {
        if(head == null ||head.next == null){
            return head;
        }
        ListNode reversedSublist = reverseList(head.next);
        head.next.next = head;
        head.next = null;
        return reversedSublist;
    }
}
```

